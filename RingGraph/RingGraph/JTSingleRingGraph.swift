//
//  JTRingGraph.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 15/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit

// MARK: Graph
@IBDesignable
public class JTSingleRingGraph: UIView {
  
  // MARK: overrides
  override public var bounds: CGRect {
    didSet{
      graphCenter = CGPoint(x: bounds.midX, y: bounds.midY)
      graphRadius = min(bounds.width, bounds.height) / 2 - selectionThickness - 1
    }
  }
  
  //MARK: public
  
  public weak var delegate: JTRingGraphDelegate?
  
  public var maximumValue: Int = 100
  
  public var startValue: Int? {
    didSet {
      if let start = startValue {
        let startPosition = CGFloat(start)/CGFloat(maximumValue)
        if oldValue == nil { self.dataLayer.strokeStart = startPosition }
        //self.animateGraphBeginingTo(start)
        self.dataLayer.strokeStart = CGFloat(start) / CGFloat(self.maximumValue)
        self.setNeedsLayout()
      }
    }
  }
  
  public var endValue: Int? {
    didSet {
      if let end = endValue {
        let endPosition = CGFloat(end)/CGFloat(maximumValue)
        if oldValue == nil { self.dataLayer.strokeEnd = endPosition }
        //self.animateGraphEndTo(end)
        self.dataLayer.strokeEnd = CGFloat(end) / CGFloat(self.maximumValue)
        self.setNeedsLayout()
      }
    }
  }
  
  // MARK: @IBInspectable
  @IBInspectable public var color: UIColor = UIColor.blackColor() {
    didSet{ self.setNeedsLayout() }
  }
  @IBInspectable public var dataColor: UIColor = UIColor.redColor() {
    didSet{
      CATransaction.begin()
      CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
      dataLayer.strokeColor = dataColor.CGColor
      self.setNeedsLayout()
      CATransaction.commit()
    }
  }
  @IBInspectable public var thickness: CGFloat = 10 {
    didSet{
      backgroundLayer.lineWidth = thickness * 2
      self.setNeedsLayout()
    }
  }
  @IBInspectable public var selectionThickness: CGFloat = 30 {
    didSet{
      dataLayer.lineWidth = selectionThickness * 2
      self.setNeedsLayout()
    }
  }
  
  // MARK: private
  
  private var graphCenter: CGPoint = CGPointZero
  private var graphRadius: CGFloat = 0
  
  private let maskLayer = CAShapeLayer()
  private let backgroundLayer = CAShapeLayer()
  private let dataLayer = CAShapeLayer()
  
  // MARK: init
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.initGraph()
  }
  
  override public func awakeFromNib() {
    self.initGraph()
    self.layoutIfNeeded()
  }
  
  override public func prepareForInterfaceBuilder() {
    self.initGraph()
  }
  
  func initGraph() {
    
    self.layoutMargins = UIEdgeInsetsZero
    
    graphCenter = CGPoint(x: bounds.midX, y: bounds.midY)
    graphRadius = min(bounds.width, bounds.height) / 2 - selectionThickness - 1
    
    self.initBackgroundRing()
    self.initMaskCircle()
    self.initDataArc()
  }
  
  func initBackgroundRing() {
    backgroundLayer.strokeColor = tintColor.CGColor
    layer.addSublayer(backgroundLayer)
  }
  
  func initMaskCircle() {
    maskLayer.lineWidth = 0
    maskLayer.strokeColor = backgroundColor?.CGColor
    maskLayer.fillColor = backgroundColor?.CGColor
    layer.insertSublayer(maskLayer, above: backgroundLayer)
  }
  
  func initDataArc() {
    dataLayer.strokeStart = 0.0
    dataLayer.strokeEnd = 0.0
    self.layer.insertSublayer(dataLayer, below: maskLayer)
  }
  
  // MARK: layout
  override public func layoutSubviews() {
    super.layoutSubviews()
    
    self.layoutBackgroundRing()
    self.layoutMask()
    self.layoutData()
  }
  
  func layoutBackgroundRing() {
    let backgroundPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: -CGFloat.pi/2, endAngle: 3*CGFloat.pi/2, clockwise: true)
    
    backgroundLayer.position = graphCenter
    backgroundLayer.path = backgroundPath.CGPath
  }
  
  func layoutMask() {
    let maskPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: CGFloat(0), endAngle: 2*CGFloat.pi, clockwise: true)
    maskPath.closePath()
    maskLayer.position = graphCenter
    maskLayer.path = maskPath.CGPath
  }
  
  func layoutData() {
    let dataPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: -CGFloat.pi/2, endAngle: 3*CGFloat.pi/2, clockwise: true)
    
    dataLayer.position = graphCenter
    dataLayer.path = dataPath.CGPath
  }
  
  // MARK: layer animations
  func animateGraphBeginingTo(position: Int) {
    
    CATransaction.begin()
    
    CATransaction.setAnimationDuration(0.5)
    CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn))
    
    self.dataLayer.strokeStart = CGFloat(position) / CGFloat(self.maximumValue)
    
    CATransaction.commit()
  }
  
  func animateGraphEndTo(position: Int) {
    
    CATransaction.begin()
    
    CATransaction.setAnimationDuration(0.5)
    CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn))
    
    self.dataLayer.strokeEnd = CGFloat(position) / CGFloat(self.maximumValue)
    
    CATransaction.commit()
  }
  
}