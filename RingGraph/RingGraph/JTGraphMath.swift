//
//  JTGraphMath.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 15/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGPoint {
  
  var description: String {
    return "(\(x), \(y))"
  }
  
  var length: CGFloat {
    return sqrt(squareLength)
  }
  
  var squareLength: CGFloat {
    return x * x + y * y
  }
  
  var unit: CGPoint {
    return self * (1.0 / length)
  }

  func relativeTo(point: CGPoint) -> CGPoint {
    return (self - point)
  }
  
  func distanceFrom(point: CGPoint) -> CGFloat {
    return (self - point).length
  }
  
  func squareDistanceFrom(point: CGPoint) -> CGFloat {
    return (self - point).squareLength
  }
  
  func angleFrom(point: CGPoint) -> CGFloat {
    return acos(cosOfAngleFrom(point))
  }
  
  func radiansFromPoint(point: CGPoint) -> CGFloat {
    return atan2(self.y - point.y, self.x - point.x)
  }
  
  func cosOfAngleFrom(point: CGPoint) -> CGFloat {
    return fmin(fmax(self * point / sqrt(self.squareLength * point.squareLength), -1.0), 1.0)
  }
  
  func nearlyEqualTo(point: CGPoint, delta: CGFloat) -> Bool {
    let difference = self - point
    return fabs(difference.x) < delta && fabs(difference.y) < delta
  }
}

prefix func + (value: CGPoint) -> CGPoint {
  return value
}

prefix func - (value: CGPoint) -> CGPoint {
  return CGPoint(x: -value.x, y: -value.y)
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
  return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (left: CGPoint, right: CGPoint) -> CGFloat {
  return left.x * right.x + left.y * right.y
}

func * (left: CGPoint, right: CGFloat) -> CGPoint {
  return CGPoint(x: left.x * right, y: left.y * right)
}

func * (left: CGFloat, right: CGPoint) -> CGPoint {
  return CGPoint(x: right.x * left, y: right.y * left)
}

func / (left: CGPoint, right: CGFloat) -> CGPoint {
  return CGPoint(x: left.x / right, y: left.y / right)
}

func == (left: CGPoint, right: CGPoint) -> Bool {
  return CGPointEqualToPoint(left, right)
}

func += (inout left: CGPoint, right: CGPoint) {
  left = left + right
}

func -= (inout left: CGPoint, right: CGPoint) {
  left = left - right
}

func *= (inout left: CGPoint, right: CGFloat) {
  left = left * right
}

func /= (inout left: CGPoint, right: CGFloat) {
  left = left / right
}

extension CGFloat {
  static var pi: CGFloat {
    return CGFloat(M_PI)
  }
}