//
//  JTRingGraph.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 15/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit

// MARK: DataSource Protocol
public protocol JTRingGraphDataSource: class {
  func maximumValueForDataInGraph(graph: JTRingGraph) -> Int
  func numberOfDataRangesInGraph(graph: JTRingGraph) -> Int
  func ringGraph(graph: JTRingGraph, getDataRangeAtIndex index: Int) -> (Int, Int)
}

// MARK: Delegate Protocol
@objc public protocol JTRingGraphDelegate: class {
  optional func ringGraph(graph: JTRingGraph, getColorForRangeAtIndex index: Int) -> CGColor
  optional func ringGraph(graph: JTRingGraph, didSelectRangeAtIndex index: Int)
  optional func ringGraph(graph: JTRingGraph, didDeselectRangeAtIndex index: Int)
}

// MARK: Graph
@IBDesignable
public class JTRingGraph: UIView {
  
  struct DataRange {
    let start: Int
    let end: Int
    let color: CGColor
  }
  
  // MARK: overrides
  override public var bounds: CGRect {
    didSet{
      graphCenter = CGPoint(x: bounds.midX, y: bounds.midY)
      graphRadius = min(bounds.width, bounds.height) / 2 - selectionThickness - 1
    }
  }
  
  //MARK: public
  public weak var datasource: JTRingGraphDataSource? {
    didSet { if self.datasource != nil { queryDataSource() } }
  }
  
  public weak var delegate: JTRingGraphDelegate?
  
  public var selectedDataRange: Int? {
    didSet {
      if selectedDataRange != nil && oldValue != selectedDataRange {
        self.delegate?.ringGraph?(self, didSelectRangeAtIndex: selectedDataRange!)
        self.selectLayerAtIndex(selectedDataRange!)
        
      }
      if oldValue != nil && oldValue != selectedDataRange {
        self.delegate?.ringGraph?(self, didDeselectRangeAtIndex: oldValue!)
        self.deselectLayerAtIndex(oldValue!)
      }
    }
  }
  
  // MARK: @IBInspectable
  @IBInspectable public var color: UIColor = UIColor.blackColor() {
    didSet{ self.setNeedsLayout() }
  }
  @IBInspectable public var dataColor: UIColor = UIColor.redColor() {
    didSet{ self.setNeedsLayout() }
  }
  @IBInspectable public var thickness: CGFloat = 10 {
    didSet{ self.setNeedsLayout() }
  }
  @IBInspectable public var selectionThickness: CGFloat = 30 {
    didSet{ self.setNeedsLayout() }
  }
  
  // MARK: private
  private var dataRanges: [DataRange]? //?
  private var maximumValue: Int = 100
  
  private var graphCenter: CGPoint = CGPointZero
  private var graphRadius: CGFloat = 0
  
  private let maskLayer = CAShapeLayer()
  private let backgroundLayer = CAShapeLayer()
  private var dataLayers: [CAShapeLayer]?
  
  // MARK: init
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.initGraph()
  }
  
  override public func awakeFromNib() {
    self.initGraph()
    self.layoutIfNeeded()
  }
  
  override public func prepareForInterfaceBuilder() {
    self.initGraph()
  }
  
  func initGraph() {
    
    self.layoutMargins = UIEdgeInsetsZero
    
    graphCenter = CGPoint(x: bounds.midX, y: bounds.midY)
    graphRadius = min(bounds.width, bounds.height) / 2 - selectionThickness - 1
    
    self.initBackgroundRing()
    self.initMaskCircle()
  }
  
  func initBackgroundRing() {
    backgroundLayer.lineWidth = thickness * 2
    backgroundLayer.strokeColor = tintColor.CGColor
    layer.addSublayer(backgroundLayer)
  }
  
  func initMaskCircle() {
    maskLayer.lineWidth = 0
    maskLayer.strokeColor = backgroundColor?.CGColor
    maskLayer.fillColor = backgroundColor?.CGColor
    layer.insertSublayer(maskLayer, above: backgroundLayer)
  }
  
  // MARK: data
  public func reloadRanges() {
    self.dataRanges = [DataRange]()
    self.cleanLayers()
    self.dataLayers = nil
    self.queryDataSource()
    self.setNeedsLayout()
  }
  
  func queryDataSource() {
    if let datasource = self.datasource {
      
      self.maximumValue = datasource.maximumValueForDataInGraph(self)
      let elementsCount = datasource.numberOfDataRangesInGraph(self)
      
      self.dataRanges = [DataRange](count: elementsCount, repeatedValue: DataRange(start: 0, end: 0, color: UIColor.clearColor().CGColor))
      self.dataLayers = [CAShapeLayer](count: elementsCount, repeatedValue: CAShapeLayer())
      
      for i in 0..<elementsCount {
        let (start, end) = datasource.ringGraph(self, getDataRangeAtIndex: i)
        let color = self.delegate?.ringGraph?(self, getColorForRangeAtIndex: i)
        let range = DataRange(start: start, end: end, color: (color ?? self.dataColor.CGColor))
        self.dataRanges?[i] = range
        
        let layer = CAShapeLayer()
        layer.lineWidth = thickness * 2
        layer.strokeColor = range.color
        dataLayers?[i] = layer
        self.layer.insertSublayer(layer, below: maskLayer)
      }
      
    }
  }
  
  // MARK: layout
  override public func layoutSubviews() {
    super.layoutSubviews()
    
    self.layoutBackgroundRing()
    self.layoutMask()
    self.layoutDataRings()
  }
  
  func layoutBackgroundRing() {
    let backgroundPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: -CGFloat.pi/2, endAngle: 3*CGFloat.pi/2, clockwise: true)
    
    backgroundLayer.position = graphCenter
    backgroundLayer.path = backgroundPath.CGPath
  }
  
  func layoutMask() {
    let maskPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: CGFloat(0), endAngle: 2*CGFloat.pi, clockwise: true)
    maskPath.closePath()
    maskLayer.position = graphCenter
    maskLayer.path = maskPath.CGPath
  }
  
  func layoutDataRings() {
    if let data = self.dataRanges,
      let layers = self.dataLayers {
        for i in 0..<data.count {
          let layer = layers[i]
          let range = data[i]
          
          let absoluteStart = CGFloat(range.start)/CGFloat(maximumValue) * CGFloat.pi*2
          let absoluteEnd = CGFloat(range.end)/CGFloat(maximumValue) * CGFloat.pi*2
          
          let zeroAngle = -CGFloat.pi/2
          let startAngle = zeroAngle + absoluteStart
          let endAngle = zeroAngle + absoluteEnd
          let dataPath = UIBezierPath(arcCenter: CGPointZero, radius: graphRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
          
          layer.position = graphCenter
          layer.path = dataPath.CGPath
          
        }
    }
  }
  
  func cleanLayers() {
    if let layers = self.dataLayers {
      for i in 0..<layers.count {
        let layer = layers[i]
        layer.removeFromSuperlayer()
      }
    }
  }
  
  // MARK: touches
  override public func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    if let touch = touches.first {
      let touchLocation = touch.locationInView(self)
      self.processTouchAtPoint(touchLocation)
    }
    super.touchesBegan(touches, withEvent: event)
  }
  
  override public func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
  }
  
  override public func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesMoved(touches, withEvent: event)
  }
  
  override public func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    super.touchesCancelled(touches, withEvent: event)
  }
  
  func processTouchAtPoint(point: CGPoint) {
    
    let distance = point.distanceFrom(graphCenter)
    
    var angle = point.radiansFromPoint(graphCenter)
    if angle < -CGFloat.pi/2 {
      angle = 2*CGFloat.pi + angle
    }
    
    // touch on graph
    var selectedDataRange: Int? = nil
    
    if let data = self.dataRanges,
      let layers = self.dataLayers {
        for i in 0..<data.count {
          
          let layer = layers[i]
          let range = data[i]
          
          let graphWidth = layer.lineWidth/2
          
          let startAngle = CGFloat(range.start)/CGFloat(maximumValue) * CGFloat.pi*2 - CGFloat.pi/2
          let endAngle = CGFloat(range.end)/CGFloat(maximumValue) * CGFloat.pi*2 - CGFloat.pi/2
          
          if graphRadius < distance && distance < graphRadius+graphWidth &&
            startAngle <= angle && angle <= endAngle {
              selectedDataRange = i
          }
        }
    }
    self.selectedDataRange = selectedDataRange
  }
  
  // layer animations
  func selectLayerAtIndex(index: Int) {
    
    if let layer = self.dataLayers?[index] {
      CATransaction.begin()
      
      CATransaction.setAnimationDuration(0.5)
      CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn))
      
      layer.lineWidth = self.selectionThickness*2
      
      CATransaction.commit()
    }
  }
  
  func deselectLayerAtIndex(index: Int) {
    
    if let layer = self.dataLayers?[index] {
      CATransaction.begin()
      
      CATransaction.setAnimationDuration(0.5)
      CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut))
      
      layer.lineWidth = self.thickness*2
      
      CATransaction.commit()
    }
  }
  
}