//
//  JTLoginViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import Foundation
import ReactiveCocoa

class JTSignupViewModel: JTReactiveViewModel {
  
  // MARK: Inputs
  var username = MutableProperty("")
  var password = MutableProperty("")
  var image = MutableProperty<UIImage?>(nil)
  
  // MARK: Outputs
  var signupButtonHidden = MutableProperty(true)
  var choosePhotoButtonHidden = MutableProperty(false)
  var userImageViewHidden = MutableProperty(true)
  
  // MARK: Private properties
  let canSignUp = MutableProperty(false)
  private let validateUsername = MutableProperty(false)
  private let validatePassword = MutableProperty(false)
  
  // MARK: Actions
  lazy var signupAction: Action<Void, String, NoError> = { [unowned self] in
    return Action(enabledIf: self.canSignUp, { _ in
      
      let username = self.username.value
      let password = self.password.value
      
      var jpegData: NSData? = nil
      if let image = self.image.value {
        jpegData = UIImageJPEGRepresentation(image, 0.5)
      }
      
      let accountProducer = JTUserService.sharedInstance.signupWithUsername(username, andPassword: password)
        .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.Signup) })
        .ignoreNil()
        .takeLast(1)
        .then( JTFileService.sharedInstance.sendData(jpegData, asType: .JPEG, withName: username+".jpg") )
        .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.Upload) })
        .flatMap(.Concat) { file in JTUserService.sharedInstance.updateUsersImage(file) }
        .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.Update) })
        .map { user in  return user != nil ? "Account created" : "error" }
        .flatMapError { _ in SignalProducer<String, NoError>.empty }
      
      return accountProducer
    })
    }()

  // MARK: init
  override init() {
    super.init()
    
    self.title = "Sign up"
    
    // username and password must be over 2 characters
    self.validateUsername <~ username.producer.map { $0.characters.count > 2 }
    self.validatePassword <~ password.producer.map { $0.characters.count > 2 }
    
    // dsable/enable login button based on validation
    self.canSignUp <~ combineLatest(validateUsername.producer, validatePassword.producer).map { (u, p) in return u && p }
    self.signupButtonHidden <~ canSignUp.producer.map { !$0 }
    
    // show user's image when available
    self.userImageViewHidden <~ image.producer.map { $0 == nil }
  }
}