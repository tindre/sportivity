//
//  JTErrorViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa

//TODO: delete
class JTErrorViewModel: JTReactiveViewModel {
  
  var error: JTParseError
  var message = MutableProperty("")
  
  init(message: JTError) {
    self.error = message
    self.message.value = error.error ?? ""
  }
  
}