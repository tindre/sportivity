//
//  JTMainViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import XCGLogger
import Timepiece
import Alamofire

// ViewModel for the main view
class JTMainViewModel: JTReactiveViewModel {
  
  // Constants
  let ringSegmentLengthInMinutes: Int = 5
  let totalRingSegments: Int = 288
  
  // MARK: Inputs
  var selectedActivity = MutableProperty(-1)
  var selectedDate = MutableProperty<NSDate?>(nil)
  
  // MARK: Outputs
  var currentActivity = MutableProperty(-1)
  var currentTime = MutableProperty<NSDate?>(nil)
  var userImage = MutableProperty<UIImage?>(nil)
  
  let logoutButtonHidden = MutableProperty(false)
  let addButtonEnaled = MutableProperty(false)
  
  // MARK: Data for collection views
  var activitiesCollectionElements = MutableProperty([JTActivity]())
  
  // MARK: Actions
  lazy var logoutAction: Action<Void, JTUser?, JTParseError> = {
    return Action { _ in
      log.debug("logout")
      let producer = JTUserService.sharedInstance.logout()
      return producer
    }
    }()
  
  lazy var addActivityAction: Action<Void, Void, NoError> = { [unowned self] in
    return Action { date in
      let date = self.selectedDate.value ?? NSDate()
      return SignalProducer<Void, NoError> { sink, disposable in
        AppRouter.sharedInstance.showAddActivity(date)
        sendCompleted(sink)
      }
    }
    }()
  
  // MARK: Init
  init(user: JTUser) {
    super.init()
    
    self.title = "Sportivity"
    
    // fetch user's image if present
    if let userImageUrl = user.image?.url {
      Alamofire.request(.GET, userImageUrl).response { (request, response, data, error) in
        if let data = data { self.userImage.value = UIImage(data: data, scale:1) }
      }
    }
    
    // select today's date for a start
    self.selectedDate.value = NSDate()
    
    // fetch and process data from parse backend
    self.activitiesCollectionElements <~ self.selectedDate.producer
      .ignoreNil()
      .map { ($0.beginningOfDay, $0.endOfDay ) }
      .promoteErrors(JTParseError)
      .flatMap(.Concat) { (from, to) in JTBackendService.sharedInstance.fetchActivitiesFrom(from, to: to) }
      .ignoreNil()
      .map { response in response.results }
      .ignoreNil()
      .flatMapError { _ in SignalProducer<[JTActivity], NoError>.empty }
    
    // populate outputs
    self.currentTime <~ self.selectedDate
    self.currentActivity <~ self.selectedActivity.producer.skipRepeats()
    
  }
  
}