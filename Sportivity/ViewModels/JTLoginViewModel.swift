//
//  JTLoginViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa

// ViewModel for Login view
class JTLoginViewModel: JTReactiveViewModel {
  
  // MARK: Inputs
  var username = MutableProperty("")
  var password = MutableProperty("")
  
  // MARK: Outputs
  var loginButtonHidden = MutableProperty(true)
  
  // MARK: Private properties
  private let canLogIn = MutableProperty(false)
  private let validateUsername = MutableProperty(false)
  private let validatePassword = MutableProperty(false)
  
  // MARK: Actions
  lazy var loginAction: Action<Void, String, NoError> = { [unowned self] in
    return Action(enabledIf: self.canLogIn, { _ in
      
      var producer = JTUserService.sharedInstance.loginWithUsername(self.username.value , andPassword: self.password.value)
        .ignoreNil()
        .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.Login) })
        .flatMapError { _ in SignalProducer<JTUser, NoError>.empty }
        .map { _ in "Logged In" }
      
      return producer
    })
    }()
  
  lazy var signupAction: Action<Void, Void, NoError> = { [unowned self] in
    return Action { _ in
      
      return SignalProducer<Void, NoError> { sink, disposable in
        AppRouter.sharedInstance.showSignUpView()
        sendCompleted(sink)
        
      }
    }
    }()
  
  // MARK: init
  override init() {
    super.init()
    
    self.title = "login"
    
    // username and password must be over 2 characters
    self.validateUsername <~ username.producer.map { $0.characters.count > 2 }
    self.validatePassword <~ password.producer.map { $0.characters.count > 2 }
    
    self.canLogIn <~ combineLatest(validateUsername.producer, validatePassword.producer).map { (u, p) in return u && p }
    
    // dsable/enable login button based on validation
    self.loginButtonHidden <~ canLogIn.producer.map { !$0 }
  }
}