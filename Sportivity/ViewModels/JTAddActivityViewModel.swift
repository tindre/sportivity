//
//  JTLoginViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Timepiece

class JTAddActivityViewModel: JTReactiveViewModel { //, UIPickerViewDataSource {
  
  // Constants
  let segmentLengthInMinutes: Int = 5
  let totalSegments: Int = 288
  let activityDate: NSDate
  
  // MARK: Inputs
  var selectedType = MutableProperty(-1)
  var selectedStartTime = MutableProperty(-1)
  var selectedEndTime = MutableProperty(-1)
  
  // MARK: Outputs
  var currentType = MutableProperty(-1)
  var startTime = MutableProperty(-1)
  var endTime = MutableProperty(-1)
  let addButtonEnaled = MutableProperty(false)
  
  // MARK: Data for collection views
  var activityTypesCollectionElements = MutableProperty([JTActivityType]())
  
  // values we are storing are minutes from 0 to 24h blocks defined by 'segmentLengthInMinutes'
  lazy var startTimeCollectionElements: MutableProperty<[Int]> = {
    
    var elements = [Int](count: self.totalSegments, repeatedValue: 0)
    for i in 0..<self.totalSegments { elements[i] = (i * self.segmentLengthInMinutes) }
    return MutableProperty(elements)
    
    }()
  
  // values we are storing are minutes from 0 to 24h blocks defined by 'segmentLengthInMinutes'
  lazy var endTimeCollectionElements: MutableProperty<[Int]> = {
    
    var elements = [Int](count: self.totalSegments, repeatedValue: 0)
    for i in 0..<self.totalSegments { elements[i] = (i * self.segmentLengthInMinutes) }
    return MutableProperty(elements)
    
    }()
  
  // MARK: Actions
  lazy var addAction: Action<Void, String, NoError> = { [unowned self] in
    return Action(enabledIf: self.addButtonEnaled, { _ in
      
      // get user entered data
      let selected = self.selectedType.value
      let type = self.activityTypesCollectionElements.value[selected]
      
      //change relative minutes from midnight to a full date
      let referenceTime = self.activityDate.beginningOfDay
      let startTime = referenceTime + self.startTime.value.minutes
      let endTime = referenceTime + self.endTime.value.minutes
      
      // query backend for results
      var producer = JTBackendService.sharedInstance.addActivity(ofType: type, fromDate: startTime, toDate: endTime)
        .ignoreNil()
        .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.AddActivity) })
        .flatMapError { _ in SignalProducer<JTParseBase, NoError>.empty }
        .on(next: { _ in AppRouter.sharedInstance.navigateBack() })
        .map { _ in "Added" } // <- may be used in a future, otherwise delete
      
      return producer
    })
    }()
  
  // Internal models
  var activityTypes = MutableProperty([JTActivityType]())
  
  // MARK: Init
  init(date: NSDate) {
    
    self.activityDate = date
    
    super.init()
    
    self.title = "Add Activity"
    
    // fetch list of activities
    self.activityTypesCollectionElements <~ JTBackendService.sharedInstance.getActivityTypes()
      .ignoreNil()
      .on(error: { error in AppRouter.sharedInstance.error.value = error.errorWithType(.AddActivity) })
      .flatMapError { _ in SignalProducer<JTParseResponse<JTActivityType>, NoError>.empty }
      .map { response in response.results }
      .ignoreNil()
    
    // calculate initial value of "start" field
    let referenceTime = activityDate.beginningOfDay
    let timeDistance = activityDate - referenceTime
    
    self.startTime.value = Int(timeDistance/60)
    
    // create stream that consists of selected start and end dates
    let selectionStream = combineLatest(self.selectedStartTime.producer, self.selectedEndTime.producer)
      .filter { (start, end) in start >= 0 }
    
    // make sure that start date is before end date
    self.startTime <~ selectionStream
      .map { (start, end) in (start, max(end, Int.max)) }
      .map { (start, end) in min(start,end) }
    
    // make sure that end date is after start date
    self.endTime <~ selectionStream.map { (start, end) in max(start,end) }
    
    self.currentType <~ self.selectedType
    
    // enable add button only if we selected all data
    let condition1 = selectedType.producer.filter { $0 >= 0}
    let condition2 = selectedStartTime.producer.filter { $0 >= 0 }
    let condition3 = selectedEndTime.producer.filter { $0 >= 0}
    
    self.addButtonEnaled <~ combineLatest(condition1, condition2, condition3).map { _ in return true }
    
  }
  
  // MARK: Helpers
  func indexToMinutes(index: Int) -> Int{
    return index * self.segmentLengthInMinutes
  }
  
  func minutesToIndex(minutes: Int) -> Int {
    return Int(minutes / self.segmentLengthInMinutes)
  }
  
}