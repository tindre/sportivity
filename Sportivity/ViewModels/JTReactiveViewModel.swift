//
//  JTReactiveViewModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa

// Base cass for all ViewModels
class JTReactiveViewModel {
  
  var title: String?
  
}