//
//  ISO8601DateTransform.swift
//  ObjectMapper
//
//  Created by Jean-Pierre Mouilleseaux on 21 Nov 2014.
//
//

import Foundation
import ObjectMapper

public class JTParseDateTransform: DateFormaterTransform {

	public init() {
		let formatter = NSDateFormatter()
		formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
		formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    formatter.timeZone = NSTimeZone(abbreviation: "UTC")
		
		super.init(dateFormatter: formatter)
	}
	
  override public func transformToJSON(value: NSDate?) -> String? {
    let result = super.transformToJSON(value)
    return result
  }
  
}
