//
//  JTUIKitExtensions.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 11/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa

extension AssociationKey {

  static var image: UInt8 = 4
  static var index: UInt8 = 5
  static var enabled: UInt8 = 6
  static var color: UInt8 = 7
  static var selected: UInt8 = 8
  static var background: UInt8 = 8
  
}

extension UIView {
  public var rac_backgroundColor: MutableProperty<UIColor?> {
    return lazyMutableProperty(self,
      key: &AssociationKey.background,
      setter: { self.backgroundColor = $0 },
      getter: { self.backgroundColor })
  }
}

extension UIControl {
  public var rac_selected: MutableProperty<Bool> {
    return lazyMutableProperty(self,
      key: &AssociationKey.selected,
      setter: { self.selected = $0 },
      getter: { self.selected })
  }
  
  public var rac_enabled: MutableProperty<Bool> {
    return lazyMutableProperty(self,
      key: &AssociationKey.enabled,
      setter: { self.enabled = $0 },
      getter: { self.enabled })
  }
}

extension UIButton {
  public var rac_text: MutableProperty<String> {
    return lazyMutableProperty(self,
      key: &AssociationKey.text,
      setter: { self.setTitle($0, forState: UIControlState.Normal) },
      getter: { self.titleForState(UIControlState.Normal) ?? "" })
  }
  
  public var rac_titleColor: MutableProperty<UIColor?> {
    return lazyMutableProperty(self,
      key: &AssociationKey.color,
      setter: { self.setTitleColor($0, forState: .Normal) },
      getter: { self.titleColorForState(.Normal) })
  }
  
}

extension UIImageView {
  public var rac_image: MutableProperty<UIImage?> {
    return lazyMutableProperty(self,
      key: &AssociationKey.image,
      setter: { self.image = $0 },
      getter: { self.image })
  }
}
