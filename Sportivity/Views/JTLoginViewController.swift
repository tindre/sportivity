//
//  JTLoginViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa

// Login screen
class JTLoginViewController: JTReactiveViewController {
  
  //MARK: IB bindings
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var signupButton: UIButton!
  
  //MARK: private properties
  private var loginAction: CocoaAction?
  private var signupAction: CocoaAction?
  
  // MARK: Bind to viewmodel
  override func bindViewModel() {
    super.bindViewModel()
    
    if let viewmodel = self.viewmodel as? JTLoginViewModel {
      
      // bind text input to viewmodel's properties
      viewmodel.username <~ self.usernameTextField.rac_text.producer.ignoreNil()
      viewmodel.password <~ self.passwordTextField.rac_text.producer.ignoreNil()
      
      //show/hide login button
      self.loginButton.rac_hidden <~ viewmodel.loginButtonHidden
      
      // login action
      self.loginAction = CocoaAction(viewmodel.loginAction, input: () )
      self.loginButton.addTarget(self.loginAction, action: CocoaAction.selector, forControlEvents: .TouchUpInside)
      self.loginButton.rac_text <~ viewmodel.loginAction.events.dematerialize()
      
      // signup action
      self.signupAction = CocoaAction(viewmodel.signupAction, input: () )
      self.signupButton.addTarget(self.signupAction, action: CocoaAction.selector, forControlEvents: .TouchUpInside)
      
    }
    
  }
  
  //MARK: helpers
  
  // detect touches on view background
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
    // hide keyboard
    view.endEditing(true)
    super.touchesBegan(touches, withEvent: event)
  }
  
}

