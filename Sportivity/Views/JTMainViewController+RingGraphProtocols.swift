//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Timepiece
import RingGraph

extension JTMainViewController: JTRingGraphDataSource, JTRingGraphDelegate  {
  
  // JTRingGraph

  // Dictates how many discrete segments 'ticks' we will have in a full circle
  // in our case it is: 24H * 60 (minutes in a day)
  // divided by length of time segment that is 5m
  // however that can be fully controlled by viewmodel
  func numberOfDataRangesInGraph(graph: JTRingGraph) -> Int {
    
    let viewmodel = self.viewmodel as! JTMainViewModel
    return viewmodel.activitiesCollectionElements.value.count
  }
  
  // Dictates what value represents 100% - full circle
  // in our case it is: 24H * 60 (minutes in a day)
  func maximumValueForDataInGraph(graph: JTRingGraph) -> Int {
    
    let viewmodel = self.viewmodel as! JTMainViewModel
    return viewmodel.totalRingSegments * viewmodel.ringSegmentLengthInMinutes
  }
  
  // returns a range (start, end) at given index
  func ringGraph(graph: JTRingGraph, getDataRangeAtIndex index: Int) -> (Int, Int) {
    
    let viewmodel = self.viewmodel as! JTMainViewModel
    let activity = viewmodel.activitiesCollectionElements.value[index]
    
    let activityDate = dateForIndex(self.selectedTime.value)
    let referenceTime = activityDate.beginningOfDay
    
    let start = (activity.startsAt.date  - referenceTime)/60
    let end = (activity.endsAt.date  - referenceTime)/60
    
    return (Int(start), Int(end))
  }
  
  // Controlls a color for a given range
  func ringGraph(graph: JTRingGraph, getColorForRangeAtIndex index: Int) -> CGColor {
    
    let viewmodel = self.viewmodel as! JTMainViewModel
    let activity = viewmodel.activitiesCollectionElements.value[index]
    
    return activity.type.category.color.CGColor
  }
  
}

