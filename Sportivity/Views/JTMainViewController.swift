//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Timepiece
import RingGraph

// MVVM View for main screen
class JTMainViewController: JTReactiveViewController {
  
  //MARK: IB bindings
  @IBOutlet weak var activityCollectionView: UICollectionView!
  @IBOutlet weak var timeCollectionView: UICollectionView!
  
  @IBOutlet weak var ringGraph: JTRingGraph!
  @IBOutlet weak var userImage: UIImageView!
  
  //MARK: Outputs for ViewModel
  let selectedTime = MutableProperty<Int>(-1)
  let selectedActivity = MutableProperty<Int>(-1)
  
  //MARK: private properties
  let refferenceTime = NSDate()
  let dataRange = 1000
  
  var logoutAction: CocoaAction?
  var addActivityAction: CocoaAction?
  
  
  //MARK: View's lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // register cells
    self.timeCollectionView.registerNib(UINib(nibName: "JKTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JKTextCollectionViewCell")
    self.activityCollectionView.registerNib(UINib(nibName: "JKTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JKTextCollectionViewCell")
    
    // set graph's delegate and datasource
    self.ringGraph.delegate = self
    self.ringGraph.datasource = self
    
    // round imageview with user's image
    userImage.layer.borderWidth = 0.0
    userImage.layer.masksToBounds = false
    userImage.layer.borderColor = JTStyleManager.styleLightColor.CGColor
    userImage.layer.cornerRadius = userImage.frame.size.width/2
    userImage.clipsToBounds = true
    
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    // when view first apperas selected date sometimes is set before view finishes laying out collection elements
    // and collectionview doesn't scroll, hence we need to check if selected item is visible and force relayout if needed
    if let selectedCell = timeCollectionView.indexPathsForSelectedItems()?.first {
      
      let visibleCells = timeCollectionView.indexPathsForVisibleItems()
      var isVisible = false
      
      for index in visibleCells {
        if index.item == selectedCell.item { isVisible = true }
      }
      
      if isVisible == false {
        self.view.layoutIfNeeded()
        self.timeCollectionView.scrollToItemAtIndexPath(selectedCell, atScrollPosition: .CenteredHorizontally, animated: false)
      }
    }
    
    // refresh view after coming back from "Add new Item"
    if let viewmodel = self.viewmodel as? JTMainViewModel,
      let date = viewmodel.currentTime.value {
        
        let currentDate = date.copy() as! NSDate
        self.selectedTime.value = self.indexFromDate(currentDate)
    }
  }
  
  // MARK: Bind to viewmodel
  override func bindViewModel() {
    super.bindViewModel()
    
    if let viewmodel = self.viewmodel as? JTMainViewModel {
      
      // bind viewmodel's image to imageview
      self.userImage.rac_image <~ viewmodel.userImage
      
      // Actions
      self.logoutAction = CocoaAction(viewmodel.logoutAction, input: () )
      let logoutButton = UIBarButtonItem(title: "Logout", style: .Plain, target: self.logoutAction, action: CocoaAction.selector)
      self.navigationItem.leftBarButtonItem = logoutButton
      
      self.addActivityAction = CocoaAction(viewmodel.addActivityAction, input: () )
      let addButton = UIBarButtonItem(title: "Add", style: .Plain, target: self.addActivityAction, action: CocoaAction.selector)
      self.navigationItem.rightBarButtonItem = addButton
      
      // bind collection views to data
      viewmodel.activitiesCollectionElements.producer
        .startWithNext { _ in
          self.activityCollectionView.reloadData()
          self.ringGraph.reloadRanges()
      }
      
      // bind view properties to viewmodel inputs
      viewmodel.selectedDate <~ self.selectedTime.producer
        .filter { $0 >= 0 }
        .map { index in self.dateForIndex(index) }
      
      viewmodel.selectedActivity <~ self.selectedActivity.producer
      
      // bind collection views current values to viewmodel outputs
      viewmodel.selectedDate.producer
        .ignoreNil()
        .startWithNext { date in
          let index = NSIndexPath(forItem: self.indexFromDate(date), inSection: 0)
          self.timeCollectionView.scrollToItemAtIndexPath(index, atScrollPosition: .CenteredHorizontally, animated: true)
          self.timeCollectionView.selectItemAtIndexPath(index, animated: true, scrollPosition: .CenteredHorizontally)
      }
      
      viewmodel.currentActivity.producer
        .skipRepeats()
        .startWithNext { index in
          if index >= 0 {
            self.activityCollectionView.selectItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), animated: true, scrollPosition: .CenteredVertically)
            self.ringGraph.selectedDataRange = index
          } else {
            if let path = self.activityCollectionView.indexPathsForSelectedItems()?.first {
              self.activityCollectionView.deselectItemAtIndexPath(path, animated: true)
              self.ringGraph.selectedDataRange = nil
            }
          }
          
      }
      
    }
    
  }
  
  //MARK: helpers
  // deselect activity whe user taps on a background
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
    if let touch = touches.first {
      
      // if the touch is within graphview
      let location = touch.locationInView(self.view)
      
      if CGRectContainsPoint(self.ringGraph.frame, location) {
        
        // check if anything is selected on the graph
        if let graphSelection = self.ringGraph.selectedDataRange {
          
          // if so make it as a selected activity
          self.selectedActivity.value = graphSelection
        } else {
          
          // nothing is selected on the graph, clear selection
          let current = min(self.selectedActivity.value, -1)
          self.selectedActivity.value = 2*current
        }
      } else {
        
        // clear selection
        let current = min(self.selectedActivity.value, -1)
        self.selectedActivity.value = 2*current
      }
    }
    super.touchesBegan(touches, withEvent: event)
  }
  
  // Helpers
  
  func dateForIndex(index: Int) -> NSDate {
    let timeShift = index - 1 - dataRange
    let realTime = timeShift >= 0 ? timeShift.days.later : abs(timeShift).days.ago
    return realTime
  }
  
  func indexFromDate(time: NSDate) -> Int {
    let referenceDate = NSDate()
    let timeDifference = referenceDate - time
    let index = dataRange + 1 - Int(timeDifference/(60*60*24))
    return index
  }
  
  func indexPathFromTime(time: Int) -> NSIndexPath {
    let i = 0 // Int(time / (viewmodel as? JTMainViewModel)!.segmentLengthInMinutes)
    return NSIndexPath(forItem: i, inSection: 0)
  }
  
}

