//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa
import RingGraph

// Adding new entry
class JTAddActivityViewController: JTReactiveViewController  {
  
  //MARK: IB bindings
  @IBOutlet weak var activityCollectionView: UICollectionView!
  @IBOutlet weak var startTimeCollectionView: UICollectionView!
  @IBOutlet weak var endTimeCollectionView: UICollectionView!
  @IBOutlet weak var addActivityButton: UIButton!
  @IBOutlet weak var ringGraph: JTSingleRingGraph!
  
  //MARK: Outputs for ViewModel
  let selectedStartTime = MutableProperty(-1)
  let selectedEndTime = MutableProperty(-1)
  let selectedActivity = MutableProperty(-1)
  
  //MARK: private properties
  private var addActivityAction: CocoaAction?
  var selectionColor = MutableProperty(JTStyleManager.selectionColor)
  
  //MARK: View's lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // register cells
    self.startTimeCollectionView.registerNib(UINib(nibName: "JKReactiveTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JKReactiveTextCollectionViewCell")
    self.endTimeCollectionView.registerNib(UINib(nibName: "JKReactiveTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JKReactiveTextCollectionViewCell")
    self.activityCollectionView.registerNib(UINib(nibName: "JKReactiveTextCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "JKReactiveTextCollectionViewCell")
  }
  
  // MARK: Bind to viewmodel
  override func bindViewModel() {
    super.bindViewModel()
    
    if let viewmodel = self.viewmodel as? JTAddActivityViewModel {
      
      // bind collection views to datasources and refresh everytime source has changed
      viewmodel.startTimeCollectionElements.producer
        .startWithNext { _ in self.startTimeCollectionView.reloadData() }
      
      viewmodel.endTimeCollectionElements.producer
        .startWithNext { _ in self.endTimeCollectionView.reloadData() }
      
      viewmodel.activityTypesCollectionElements.producer
        .startWithNext { _ in self.activityCollectionView.reloadData() }
      
      // setup graph view
      self.ringGraph.maximumValue = viewmodel.totalSegments * viewmodel.segmentLengthInMinutes
      
      // bind view properties to viewmodel inputs
      viewmodel.selectedStartTime <~ self.selectedStartTime.producer.filter { $0 >= 0 }
      viewmodel.selectedEndTime <~ self.selectedEndTime.producer.filter { $0 >= 0 }
      viewmodel.selectedType <~ self.selectedActivity.producer.filter { $0 >= 0 }
      
      // bind collection views current values to viewmodel outputs
      viewmodel.startTime.producer
        .filter { $0 >= 0 }
        .take(1)
        .startWithNext { index in
          self.startTimeCollectionView.scrollToItemAtIndexPath(self.indexPathFromTime(index),
            atScrollPosition: .CenteredHorizontally,
            animated: false)
          self.endTimeCollectionView.scrollToItemAtIndexPath(self.indexPathFromTime(index),
            atScrollPosition: .CenteredHorizontally,
            animated: false)
      }
      
      viewmodel.startTime.producer
        .filter { $0 >= 0 }
        .skip(1)
        .startWithNext { index in
          self.startTimeCollectionView.selectItemAtIndexPath(self.indexPathFromTime(index), animated: true, scrollPosition: .CenteredHorizontally)
          self.ringGraph.startValue = index
      }
      
      viewmodel.endTime.producer
        .filter { $0 >= 0 }
        .startWithNext { index in
          self.endTimeCollectionView.selectItemAtIndexPath(self.indexPathFromTime(index), animated: true, scrollPosition: .CenteredHorizontally)
          self.ringGraph.endValue = index
      }
      
      viewmodel.currentType.producer
        .filter { $0 >= 0 }
        .startWithNext { index in
          self.activityCollectionView.selectItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), animated: true, scrollPosition: .CenteredVertically)
          let selectionColor = viewmodel.activityTypesCollectionElements.value[index].category.color
          self.selectionColor.value = selectionColor
      }
      
      // change UI color to curently selected activity
      self.addActivityButton.rac_titleColor <~ self.selectionColor.producer.map { Optional<UIColor>($0) }
      self.selectionColor.producer
        .startWithNext { color in self.ringGraph.dataColor = color }
      
      // add action
      self.addActivityButton.rac_hidden <~ viewmodel.addButtonEnaled.producer.map { !$0 }
      self.addActivityAction = CocoaAction(viewmodel.addAction, input: () )
      self.addActivityButton.addTarget(self.addActivityAction, action: CocoaAction.selector, forControlEvents: .TouchUpInside)
    }
    
  }
  
  // MARK: helpers
  func timeForIndex(index: Int) -> Int {
    return index * (viewmodel as? JTAddActivityViewModel)!.segmentLengthInMinutes
  }
  
  func indexFromTime(time: Int) -> Int {
    return Int(time / (viewmodel as? JTAddActivityViewModel)!.segmentLengthInMinutes)
  }
  
  func indexPathFromTime(time: Int) -> NSIndexPath {
    let i = Int(time / (viewmodel as? JTAddActivityViewModel)!.segmentLengthInMinutes)
    return NSIndexPath(forItem: i, inSection: 0)
  }
  
}

