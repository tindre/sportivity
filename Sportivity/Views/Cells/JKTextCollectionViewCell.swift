//
//  JKReactiveTextCollectionViewCell.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 14/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit

// Basic Collectionview cell with text label
class JKTextCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var label: UILabel!
  
  // public interface
  var selectionBackgroundColor: UIColor? {
    didSet { updateColors() }
  }

  var selectionTextColor: UIColor? {
    didSet { updateColors() }
  }
  
  var normalBackgroundColor: UIColor? {
    didSet { updateColors() }
  }
  
  var normalTextColor: UIColor? {
    didSet { updateColors() }
  }
  
  // overrides
  override var selected: Bool  {
    didSet { updateColors() }
  }
  
  override func prepareForReuse() {
    self.selectionBackgroundColor = nil
    self.normalBackgroundColor = nil
    self.normalTextColor = nil
    self.selectionTextColor = nil
    self.label.text = ""
    self.selected = false
  }
  
  // private functions
  private func updateColors() {
    self.backgroundColor = self.selected ? (selectionBackgroundColor ?? JTStyleManager.selectionColor) : (normalBackgroundColor ?? UIColor.clearColor())
    self.label.textColor = self.selected ? (selectionTextColor ?? JTStyleManager.backgroundColor) :
      (normalTextColor ?? JTStyleManager.tintColor)
  }
}