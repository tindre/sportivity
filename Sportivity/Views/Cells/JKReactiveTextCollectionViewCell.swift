//
//  JKReactiveTextCollectionViewCell.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 14/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa

// Reactive collection view cell
// it will have it's background bound with external reacive property
// so we can change background color of any cell without reloading collection view
class JKReactiveTextCollectionViewCell: JKTextCollectionViewCell {
  
  var selectionColor = MutableProperty<UIColor>(JTStyleManager.selectionColor)
  
  override func awakeFromNib() {
    selectionColor.producer.startWithNext { color in
      self.selectionBackgroundColor = color
    }
  }

}