//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa

extension JTAddActivityViewController: UICollectionViewDataSource, UICollectionViewDelegate  {
  
  // UICollectionView
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    var count = 0
    
    if collectionView === startTimeCollectionView {
      count = (viewmodel as? JTAddActivityViewModel)?.startTimeCollectionElements.value.count ?? 0
    }
    
    if collectionView === endTimeCollectionView {
      count = (viewmodel as? JTAddActivityViewModel)?.endTimeCollectionElements.value.count ?? 0
    }
    
    if collectionView === activityCollectionView {
      count = (viewmodel as? JTAddActivityViewModel)?.activityTypesCollectionElements.value.count ?? 0
    }
    
    return count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    let viewmodel = self.viewmodel as! JTAddActivityViewModel
    
    // get start time cell
    if collectionView === startTimeCollectionView {
      let cell = self.startTimeCollectionView.dequeueReusableCellWithReuseIdentifier("JKReactiveTextCollectionViewCell", forIndexPath: indexPath) as! JKReactiveTextCollectionViewCell
      
      // we store times as minutes from midnight
      let modelTime = Double(viewmodel.startTimeCollectionElements.value[indexPath.row])
      
      // so we need to convert them to HH:MM format
      let time = modelTime ?? Double(0)
      let hh = Int(floor(time/60))
      let mm = Int(time%60)
      cell.label.text = String(format:"%.2d:%.2d", hh, mm)
      
      // this binds cell's background color to selection color
      // it will change without reloading
      cell.selectionColor <~ self.selectionColor
      
      return cell
    }
    
    // we get end time cell
    if collectionView === endTimeCollectionView {
      let cell = self.endTimeCollectionView.dequeueReusableCellWithReuseIdentifier("JKReactiveTextCollectionViewCell", forIndexPath: indexPath) as! JKReactiveTextCollectionViewCell
      
      // we store times as minutes from midnight
      let modelTime = Double(viewmodel.startTimeCollectionElements.value[indexPath.row])
      
      // so we need to convert them to HH:MM format
      let time = modelTime ?? Double(0)
      let hh = Int(floor(time/60))
      let mm = Int(time%60)
      cell.label.text = String(format:"%.2d:%.2d", hh, mm)
      
      // this binds cell's background color to selection color
      // it will change without reloading
      cell.selectionColor <~ self.selectionColor
      
      return cell
    }
    
    // we get activity cell
    if collectionView === activityCollectionView {
      let cell = self.activityCollectionView.dequeueReusableCellWithReuseIdentifier("JKReactiveTextCollectionViewCell", forIndexPath: indexPath) as! JKReactiveTextCollectionViewCell
      
      let activityType = viewmodel.activityTypesCollectionElements.value[indexPath.row]
      cell.label.text = activityType.name.rawValue
      
      // this binds cell's background color to selection color
      // it will change without reloading
      cell.selectionColor <~ self.selectionColor
      
      return cell
    }
    
    return UICollectionViewCell()
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    if collectionView === startTimeCollectionView { selectedStartTime.value = timeForIndex(indexPath.row) }
    if collectionView === endTimeCollectionView { selectedEndTime.value = timeForIndex(indexPath.row) }
    if collectionView === activityCollectionView { selectedActivity.value = indexPath.row }
  }
  
}

