//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Timepiece

extension JTMainViewController: UICollectionViewDataSource, UICollectionViewDelegate {
  
  // UICollectionView
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    var count = 0
    
    if collectionView === timeCollectionView {
      count = 1 + 2 * dataRange
    }
    
    if collectionView === activityCollectionView {
      count = (viewmodel as? JTMainViewModel)?.activitiesCollectionElements.value.count ?? 0
    }
    
    return count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    
    let viewmodel = self.viewmodel as! JTMainViewModel
    
    // date display
    if collectionView === timeCollectionView {
      let cell = self.timeCollectionView.dequeueReusableCellWithReuseIdentifier("JKTextCollectionViewCell", forIndexPath: indexPath) as! JKTextCollectionViewCell
      
      // ok we don't need to store values anywhere because we can easily calculate what date shuld be at e given index
      let cellTime = self.dateForIndex(indexPath.item)
      
      let yy = cellTime.year
      let mm = cellTime.month
      let dd = cellTime.day
      cell.label.text = String(format:"%d.%.2d.%.2d", yy, mm, dd)
      
      return cell
    }
    
    // Activities display
    if collectionView === activityCollectionView {
      let cell = self.activityCollectionView.dequeueReusableCellWithReuseIdentifier("JKTextCollectionViewCell", forIndexPath: indexPath) as! JKTextCollectionViewCell
      
      let activity = viewmodel.activitiesCollectionElements.value[indexPath.row]
      cell.label.text = activity.type.name.rawValue
      
      // each activity has a category and each category has it's own color
      cell.selectionBackgroundColor = activity.type.category.color
      cell.normalTextColor = activity.type.category.color
      cell.selectionTextColor = JTStyleManager.backgroundColor
      
      return cell
    }
    
    // if we ever get here we have crash :(
    return UICollectionViewCell()
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    if collectionView === timeCollectionView { selectedTime.value = indexPath.row }
    if collectionView === activityCollectionView { selectedActivity.value = indexPath.row }
  }
  
}

