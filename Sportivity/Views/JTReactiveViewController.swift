//
//  JTReactiveViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa

// All classes must implement this protocol
protocol JTBindableView {
  func bindViewModel()
}

// Main "ViewController" class equivalent ov "View" in MVVM
class JTReactiveViewController: UIViewController, JTBindableView {
  
  var viewmodel: JTReactiveViewModel?
  
  //MARK: init
  // load default view with a given viewmodel
  convenience init(viewmodel: JTReactiveViewModel?) {
    
    // get class' name and load the interface from a ClassName.xib file
    let nibName = NSStringFromClass(self.dynamicType).componentsSeparatedByString(".").last
    self.init(nibName: nibName, bundle: nil)
    
    self.viewmodel = viewmodel
  }
  
  // stock initialisers
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  //MARK: Bind viewmodel
  func bindViewModel() {
    self.title = self.viewmodel?.title ?? ""
  }
  
  // UIViewController stuff
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // bnd model
    self.bindViewModel()
    
    // configure navigation bar
    self.navigationController?.navigationBar.barTintColor = JTStyleManager.backgroundColor
    self.navigationController?.navigationBar.translucent = false
    self.navigationController?.navigationBar.barStyle = UIBarStyle.Black
    self.navigationController?.navigationBar.tintColor = JTStyleManager.styleLightColor
    
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarPosition: .Any, barMetrics: .Default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    
    let backIcon = UIImage(named: "icon-back")
    self.navigationController?.navigationBar.backIndicatorImage = backIcon
    self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = backIcon
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
    
    self.navigationController?.navigationBar.titleTextAttributes = [
      NSForegroundColorAttributeName: JTStyleManager.styleLightColor,
      NSFontAttributeName: JTStyleManager.headerFont
    ]
  }
  
  // navigation helper
  func navigateBack(){
    AppRouter.sharedInstance.navigateBack()
  }
}