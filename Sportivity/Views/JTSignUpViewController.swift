//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Toucan

// Signup screen
class JTSignUpViewController: JTReactiveViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

  //MARK: IB bindings
  @IBOutlet weak var photoButton: UIButton!
  @IBOutlet weak var userPhoto: UIImageView!
  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var signupButton: UIButton!
  
  //MARK: private properties
  private var chooseUserPhotoAction: CocoaAction?
  private var signupAction: CocoaAction?
  
  // camera picker to let user make an user photo
  private lazy var picker: UIImagePickerController = {
    let picker = UIImagePickerController()
    picker.delegate = self
    picker.allowsEditing = false
    picker.sourceType = .Camera
    picker.cameraCaptureMode = .Photo
    picker.modalPresentationStyle = .FullScreen
    return picker
    }()
  
  //MARK: View lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // making user's image view round
    userPhoto.layer.borderWidth = 2.0
    userPhoto.layer.masksToBounds = false
    userPhoto.layer.borderColor = JTStyleManager.styleLightColor.CGColor
    userPhoto.layer.cornerRadius = userPhoto.frame.size.width/2
    userPhoto.clipsToBounds = true
  }
  
  //MARK: bind to viewmodel
  override func bindViewModel() {
    super.bindViewModel()
    
    if let viewmodel = self.viewmodel as? JTSignupViewModel {
      
      // bind text input to viewmodel's properties
      viewmodel.username <~ self.usernameTextField.rac_text.producer.ignoreNil()
      viewmodel.password <~ self.passwordTextField.rac_text.producer.ignoreNil()
      
      //show/hide buttons
      self.signupButton.rac_hidden <~ viewmodel.signupButtonHidden
      self.photoButton.rac_hidden <~ viewmodel.choosePhotoButtonHidden
      self.userPhoto.rac_hidden <~ viewmodel.userImageViewHidden
     
      self.userPhoto.rac_image <~ viewmodel.image
      
      // signup action
      self.signupAction = CocoaAction(viewmodel.signupAction, input: () )
      self.signupButton.addTarget(self.signupAction, action: CocoaAction.selector, forControlEvents: .TouchUpInside)
      self.signupButton.rac_text <~ viewmodel.signupAction.events.dematerialize()

    }
    
  }
  
  //MARK: Image picker
  @IBAction func presentImagePickController(sender: AnyObject) {
    if UIImagePickerController.availableCaptureModesForCameraDevice(.Front) != nil {
      self.presentViewController(picker, animated: true, completion: nil)
    }
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
    if let viewmodel = self.viewmodel as? JTSignupViewModel {
      
      // we have an image from user
      let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage

      // we make it smaller
      let processedImage = Toucan(image: chosenImage!).resize(CGSize(width: 300, height: 300), fitMode: .Crop)
      
      // and send it to viewmodel
      viewmodel.image.value = processedImage.image
    }
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  //MARK: helpers
  
  // detect touches on view background
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    
    // hide keyboard
    view.endEditing(true)
    super.touchesBegan(touches, withEvent: event)
  }
  
}

