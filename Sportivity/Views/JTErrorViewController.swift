//
//  ViewController.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import UIKit
import ReactiveCocoa

// Displays an error view
class JTErrorViewController: JTReactiveViewController {

  @IBOutlet weak var messageLabel: UILabel!

  @IBOutlet weak var iconView: UIImageView!

  @IBOutlet weak var actionButton: UIButton!
  
  override func bindViewModel() {
    super.bindViewModel()
    
    if let viewmodel = self.viewmodel as? JTErrorViewModel {
      
      self.messageLabel.rac_text <~ viewmodel.message
      
    }
    
  }
}

