//
//  JTParseTypes.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 06/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveCocoa

// MARK Error Types
enum JTErrorType {
  case NoConnection
  case User
  case Other
  case AddActivity
  case Login
  case Signup
  case Upload
  case Update
}

// MARK: Error Type
// error model that is represents Parse error message
// and is compatible with ReactiveCocoa
class JTError: JTParseError {
  
  // new instance from NSError
  class func withError(error:NSError, ofType type: JTErrorType) -> JTParseError {
    let jtError = JTError(type: type)
    jtError.withError(error)
    return jtError
  }
  
  let type: JTErrorType?
  
  // ReactiveCocoa ErrorType protocol
  var nsError: NSError {
    get {
      let errorCode = self.code ?? 0
      let errorMessage = self.error ?? "NsError"
      return NSError(domain: "JTError",
        code: errorCode,
        userInfo: [NSLocalizedDescriptionKey: errorMessage])
    }
  }
  
  // MARK: Init
  init(fromError error:NSError?, ofType type: JTErrorType) {
    self.type = type
    
    super.init()
    self.code = error?.code
    self.error = (error?.userInfo[NSLocalizedDescriptionKey] as? String )
  }
  
  init(type: JTErrorType) {
    self.type = type
    super.init()
  }
  
  override init() {
    self.type = nil
    super.init()
  }
  
  required init?(_ map: Map) {
    self.type = nil
    super.init(map)
  }
  
  // Methodes
  func withError(error:NSError) {
    self.code = error.code
    self.error = (error.userInfo[NSLocalizedDescriptionKey] as? String )
  }
}

// Model of error response from Parse backend
class JTParseError: Mappable, ErrorType {
  
  var code: Int?
  var error: String?
  
  init(code: Int, error: String){
    self.code = code
    self.error = error
  }
  
  init() {
    
  }
  
  required init?(_ map: Map){
  }
  
  func mapping(map: Map) {
    code  <- map["code"]
    error  <- map["error"]
  }
  
  func errorWithType(type: JTErrorType) -> JTError {
    let error = JTError(type: type)
    error.code = self.code
    error.error = self.error
    return error
  }
}
