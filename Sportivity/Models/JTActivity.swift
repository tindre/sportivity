//
//  JTActivityModel.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ObjectMapper

// Model of Parse object "ActivityType"
class JTActivityType: JTParseBase {
  
  var name: ActivityNames!
  var category: ActivityCategories!
  
  required init?(_ map: Map){
    super.init(map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map)
    
    name      <- map["name"]
    category  <- map["category"]
  }
  
}

// Model of Parse Object Activity with type field unwrapped
class JTActivity: JTParseBase {
  
  var user: JTParsePointer?
  var startsAt: JTParseDate!
  var endsAt: JTParseDate!
  var type: JTActivityType!
  
  required init?(_ map: Map){
    super.init(map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map)
    
    user      <- map["user"]
    startsAt  <- map["startsAt"]
    endsAt    <- map["endsAt"]
    type      <- map["type"]
  }
  
}

// Model of Parse object Activity with type field as a Pointer
// Needed for creating new Activity models
class JTParseActivity: JTParseBase {
  
  var user: JTParsePointer?
  var startsAt: JTParseDate!
  var endsAt: JTParseDate!
  var type: JTParsePointer?
  
  override init() {
    super.init()
  }
  
  required init?(_ map: Map){
    super.init(map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map)
    
    user      <- map["user"]
    startsAt  <- map["startsAt"]
    endsAt    <- map["endsAt"]
    type      <- map["type"]
  }
  
}

// MARK: List of Categories
enum ActivityCategories: String {
  case Sport = "Sport"
  case Exercise = "Exercise"
  case Aerobic = "Aerobic"
  case Anaerobic = "Anaerobic"
  case Flexibility = "Flexibility"
  case Outdoor = "Outdoor"
  case Indoor = "Indoor"
  case Other = "Other"
}

// MARK: List of Activities
enum ActivityNames: String {
  case AmericanFootball = "AmericanFootball"
  case Archery = "Archery"
  case AustralianFootball = "AustralianFootball"
  case Badminton = "Badminton"
  case Baseball = "Baseball"
  case Basketball = "Basketball"
  case Bowling = "Bowling"
  case Boxing = "Boxing"
  case Climbing = "Climbing"
  case Cricket = "Cricket"
  case CrossTraining = "CrossTraining"
  case Curling = "Curling"
  case Cycling = "Cycling"
  case Dance = "Dance"
  case DanceInspiredTraining = "DanceInspiredTraining"
  case Elliptical = "Elliptical"
  case EquestrianSports = "EquestrianSports"
  case Fencing = "Fencing"
  case Fishing = "Fishing"
  case FunctionalStrengthTraining = "FunctionalStrengthTraining"
  case Golf = "Golf"
  case Gymnastics = "Gymnastics"
  case Handball = "Handball"
  case Hiking = "Hiking"
  case Hockey = "Hockey"
  case Hunting = "Hunting"
  case Lacrosse = "Lacrosse"
  case MartialArts = "MartialArts"
  case MindAndBody = "MindAndBody"
  case MixedMetabolicCardioTraining = "MixedMetabolicCardioTraining"
  case PaddleSports = "PaddleSports"
  case Play = "Play"
  case PreparationAndRecovery = "PreparationAndRecovery"
  case Racquetball = "Racquetball"
  case Rowing = "Rowing"
  case Rugby = "Rugby"
  case Running = "Running"
  case Sailing = "Sailing"
  case SkatingSports = "SkatingSports"
  case SnowSports = "SnowSports"
  case Soccer = "Soccer"
  case Softball = "Softball"
  case Squash = "Squash"
  case StairClimbing = "StairClimbing"
  case SurfingSports = "SurfingSports"
  case Swimming = "Swimming"
  case TableTennis = "TableTennis"
  case Tennis = "Tennis"
  case TrackAndField = "TrackAndField"
  case TraditionalStrengthTraining = "TraditionalStrengthTraining"
  case Volleyball = "Volleyball"
  case Walking = "Walking"
  case WaterFitness = "WaterFitness"
  case WaterPolo = "WaterPolo"
  case WaterSports = "WaterSports"
  case Wrestling = "Wrestling"
  case Yoga = "Yoga"
  case Other = "Other"
}