//
//  JTParseTypes.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 06/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ObjectMapper

//MARK: Supported Parse objects
enum JTParseTypes: String {
  case File = "File"
  case Object = "Object"
  case Pointer = "Pointer"
  case Date = "Date"
}

// MARK: Base models
// Parse fields common for al objects
class JTParseBase: Mappable {
  
  var objectId: String!
  var createdAt: NSDate?
  var updatedAt: NSDate?
  
  init(){
  }
  
  required init?(_ map: Map){
  }
  
  func mapping(map: Map) {
    objectId    <- map["objectId"]
    createdAt   <- (map["createdAt"], JTParseDateTransform())
    updatedAt   <- (map["updatedAt"], JTParseDateTransform())
  }
  
}

// Generic model of a response from a Parse Backend
class JTParseResponse<T: Mappable>: Mappable {
  
  var results: Array<T>?
  
  required init?(_ map: Map){
  }
  
  func mapping(map: Map) {
    results <- map["results"]
  }
  
}

// respnse holding file info
struct JTParseFile: Mappable {
  
  var type: JTParseTypes?
  var name: String!
  var url: NSURL?
  
  init(type: JTParseTypes, name: String, url: NSURL? = nil) {
    self.type = type
    self.name = name
    self.url = url
  }
  
  
  init?(_ map: Map){
  }
  
  mutating func mapping(map: Map) {
    type  <- map["__type"]
    name  <- map["name"]
    url   <- (map["url"], URLTransform())
  }

}

// Pointer to another Object / Class at parse backend
struct JTParsePointer: Mappable {
  
  var type: JTParseTypes!
  var className: String!
  var objectId: String!
  
  init(type: JTParseTypes, className: String, objectId: String) {
    self.type = type
    self.className = className
    self.objectId = objectId
  }
  
  init?(_ map: Map){
  }
  
  mutating func mapping(map: Map) {
    type        <- map["__type"]
    className   <- map["className"]
    objectId    <- map["objectId"]
  }
  
}

// response holding date info
struct JTParseDate: Mappable {
  
  var type: JTParseTypes!
  var date: NSDate!
  
  init?(_ map: Map){
  }
  
  init(type: JTParseTypes, date: NSDate) {
    self.type = type
    self.date = date
  }
  
  mutating func mapping(map: Map) {
    type  <- map["__type"]
    date  <- (map["iso"], JTParseDateTransform())
  }
  
}
