//
//  JTUser.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ObjectMapper

// Model of a user object on the Parse backend
class JTUser: JTParseBase {
  
  var email: String?
  var username: String!
  var image: JTParseFile?
  var sessionToken: String?
  
  required init?(_ map: Map){
    super.init(map)
  }
  
  override func mapping(map: Map) {
    super.mapping(map)
    
    email         <- map["email"]
    username      <- map["username"]
    image         <- map["image"]
    sessionToken  <- map["sessionToken"]
  }

}