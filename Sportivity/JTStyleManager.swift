//
//  JTStyleManager.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 16/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit

// Definitions of colors used in app
class JTStyleManager {
  
  static let backgroundColor = { UIColor(hex: "#1E242E")}()
  static let textColor = { UIColor(hex: "#C1E6D5")}()
  static let tintColor = { UIColor(hex: "#C1E6D5")}()
  static let selectionColor = { UIColor(hex: "#FBB004")}()
  
  static let styleDarkColor = { UIColor(hex: "#1E242E")}()
  static let styleBlueColor = { UIColor(hex: "#4046B9")}()
  static let styleBlueGreenColor = { UIColor(hex: "#16A8CD")}()
  static let styleYellowColor = { UIColor(hex: "#FBB004")}()
  static let styleLatteColor = { UIColor(hex: "#849C93")}()
  static let styleLightColor = { UIColor(hex: "#C1E6D5")}()
  static let lightBlueColor = { UIColor(hex: "#88A9DD")}()
  
  // palette from https://dribbble.com/shots/2203485-Color-Palette
  static let styleColor1 = { UIColor(hex: "#059ece")}()
  static let styleColor2 = { UIColor(hex: "#42c697")}()
  static let styleColor3 = { UIColor(hex: "#4bc8dd")}()
  static let styleColor4 = { UIColor(hex: "#916fb5")}()
  static let styleColor5 = { UIColor(hex: "#ef6c6c")}()
  static let styleColor6 = { UIColor(hex: "#f4b571")}()
  static let styleColor7 = { UIColor(hex: "#d6639d")}()
  static let styleColor8 = { UIColor(hex: "#f7db63")}()
  static let styleColor9 = { UIColor(hex: "#6accc0")}()
  static let styleColor10 = { UIColor(hex: "#4b74b5")}()
  
  static let headerFont = { UIFont(name: "HelveticaNeue-Thin", size: 24)! }()
}

extension ActivityCategories {
 
  var color: UIColor {
    get {
      switch self {
      case .Sport: return JTStyleManager.styleColor1
      case .Exercise: return JTStyleManager.styleColor2
      case .Aerobic: return JTStyleManager.styleColor3
      case .Anaerobic: return JTStyleManager.styleColor4
      case .Flexibility: return JTStyleManager.styleColor5
      case .Outdoor: return JTStyleManager.styleColor6
      case .Indoor: return JTStyleManager.styleColor7
      case .Other: return JTStyleManager.styleColor8
      }
    }
  }
  
}