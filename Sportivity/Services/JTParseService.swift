//
//  JTUserService.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import ObjectMapper

// MARK: Parse Service
// this is base class responsible for communicating with Parse backend over REST api
class JTParseService {
  
  // MARK: Internal enumerations
  enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
  }
  
  // Public properties
  var isBusy = MutableProperty(false)
  var error = MutableProperty<ErrorType?>(nil)
  
  // Private constants
  private let parseApplicationId = "sYq7lX70KPJblYRP7BMSqP5d8B6qlko9fAQ9njMY"
  private let parseApiKey = "C0QxsNpXzQJDz6aUPxUHRjhIRX78uXEn8rXHKqeX"
  
  private let apiRootPath = "https://api.parse.com/1/"
  
  // MARK: Request formatters
  // new GET request to an endpoint with parameters passes as dictionary
  func newRequestFor(endpoint: String, withParameters parameters: [String: String]?) -> NSMutableURLRequest {
    
    var url = apiRootPath + endpoint
    
    if let parameters = parameters {
      if parameters.count > 0 { url += "?" }
      
      for (key, value) in parameters {
        url += "\(key)=\(value)&"
      }
      
      url.removeAtIndex(url.endIndex.predecessor())
    }
    
    url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) ?? ""
    
    return newRequest(HTTPMethod: .GET, url: url, data: nil)
  }
  
  // new PUT request to an endpoint with data in JSON format
  func newPUTRequestFor(endpoint: String, withJSONString json: String?) -> NSMutableURLRequest {
    
    let url = apiRootPath + endpoint
    let data = json?.dataUsingEncoding(NSUTF8StringEncoding)
    
    return newRequest(HTTPMethod: .PUT, url: url, data: data)
  }
  
  // a helper that converts JSON data into NSData and calls newPOSTRequestFor(,withData:)
  func newPOSTRequestFor(endpoint: String, withJSONString json: String?) -> NSMutableURLRequest {
    
    let data = json?.dataUsingEncoding(NSUTF8StringEncoding)
    
    return newPOSTRequestFor(endpoint, withData: data)
  }
  
  // new POST request to an endpoint with data pased in HTTPbody
  func newPOSTRequestFor(endpoint: String, withData data: NSData?) -> NSMutableURLRequest {
    
    let url = apiRootPath + endpoint
    
    return newRequest(HTTPMethod: .POST, url: url, data: data)
  }
  
  // a common function to construct request to the Parse Backend
  func newRequest(HTTPMethod method: HTTPMethod, url: String, data: NSData?) -> NSMutableURLRequest {
    
    let newRequest = NSMutableURLRequest()
    
    newRequest.HTTPMethod = method.rawValue
    newRequest.URL = NSURL(string: url)
    newRequest.setValue("application/json", forHTTPHeaderField: "Accept")
    newRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-type")
    newRequest.setValue(parseApplicationId, forHTTPHeaderField: "X-Parse-Application-Id")
    newRequest.setValue(parseApiKey, forHTTPHeaderField: "X-Parse-REST-API-Key")
    
    newRequest.HTTPBody = data
    
    return newRequest
  }
  
  // MARK: Backend request
  // generic function that queries backend and return ObjectMapper object
  func makeRequest<T: Mappable>(newRequest: NSURLRequest) -> SignalProducer<T?, JTParseError> {
    log.debug("\(newRequest.HTTPMethod!) : \(newRequest.URLString)")
    
    isBusy.value = true
    
    let producer = SignalProducer<T?, JTParseError> { sink, disposable in
      
      let request = Alamofire.request(newRequest)
        .responseString { (request , response, result) in
          
          self.isBusy.value = false
          
          switch result {
          case .Success(let jsonResponse):
            
            if let statusCode = response?.statusCode where statusCode >= 400,
              let parseError = Mapper<JTParseError>().map(jsonResponse) {
                
                // we have received 4XX error with message in JSON format
                sendError(sink, parseError)
                
            } else if let statusCode = response?.statusCode where statusCode >= 200 && statusCode < 300,
              let result = Mapper<T>().map(jsonResponse) {
                
                // we have received, and succesfully parsed, response
                sendNext(sink, result)
                sendCompleted(sink)
                
            }
          case .Failure(let data, let error):
            
            // connection error
            sendNext(sink, nil)
            sendError(sink, JTParseError())
            
          }
    
      }
      
      request.resume()
      disposable.addDisposable {
        self.isBusy.value = false
        request.cancel()
      }
    }
    
    return producer
    
  }
  
}