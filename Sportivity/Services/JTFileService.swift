//
//  JTBackendService.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import ObjectMapper

// MARK: Public enumeration
enum JTParseFileContentType: String {
  case JPEG = "image/jpeg"
}

// MARK: File upload service
class JTFileService: JTParseService {
  
  // Public properties
  static let sharedInstance = JTFileService()
  
  // Private constants
  private let apiEndpoint = "files/"
  
  // sends file to Parse, file is not automatically bound to any object so it must be handled somewhere else
  func sendData(data: NSData?, asType type: JTParseFileContentType, withName name: String) -> SignalProducer<JTParseFile?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTParseFile?, JTParseError>
    
    if let data = data {
      
      let endpoint = apiEndpoint + name
      let newRequest = newPOSTRequestFor(endpoint, withData: data)
      newRequest.setValue(type.rawValue, forHTTPHeaderField: "Content-type")
      requestProducer = makeRequest(newRequest)
      
    } else {
      
      // that should be an error however that would break our chain on signup call hence we return nil to indicate error
      // as we dont want to count file upload error as whole sign up error
      requestProducer = SignalProducer<JTParseFile?, JTParseError> { sink, disposable in
        sendNext(sink, nil)
        sendCompleted(sink)
      }
      
    }
    
    return requestProducer
  }
  
}
