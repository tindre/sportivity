//
//  JTBackendService.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import ObjectMapper

// MARK: Query
struct JTParseActivitiesBackendQuery: Mappable {
  
  var user: JTParsePointer?
  var fromDate: JTParseDate?
  var toDate: JTParseDate?
  
  init() {
    
  }
  
  init?(_ map: Map){
  }
  
  mutating func mapping(map: Map) {
    user      <- map["user"]
    fromDate  <- map["startsAt.$gte"]
    toDate    <- map["startsAt.$lte"]
  }
  
}

// MARK: Activities service
class JTBackendService: JTParseService {
  
  // Singleton
  static let sharedInstance = JTBackendService()
  
  // Private constants
  private let activitiesEndpoint = "classes/Activities"
  private let activityTypesEndpoint = "classes/ActivityType"
  
  // MARK: Public API
  // gets activities for currently logged user for a given time range
  func fetchActivitiesFrom(fromDate: NSDate, to: NSDate?) -> SignalProducer<JTParseResponse<JTActivity>?, JTParseError> {
    
    let requestProducer: SignalProducer<JTParseResponse<JTActivity>?, JTParseError>
    
    if let userId = JTUserService.sharedInstance.user.value?.objectId {
      
      let toDate = to ?? (NSDate.distantFuture() )
      
      var parameters = [String: String]()
      
      var q = JTParseActivitiesBackendQuery()
      q.user = JTParsePointer(type: .Pointer, className: "_User", objectId: userId)
      q.fromDate = JTParseDate(type: .Date, date: fromDate)
      q.toDate = JTParseDate(type: .Date, date: toDate)
      
      if let query = Mapper().toJSONString(q, prettyPrint: false) {
        parameters["where"] = query
      }
      
      parameters["include"] = "type"
      
      let newRequest = newRequestFor(activitiesEndpoint, withParameters: parameters)
      
      requestProducer = makeRequest(newRequest)
      
    } else {
      
      requestProducer = SignalProducer<JTParseResponse<JTActivity>?, JTParseError> { sink, _ in
        sendError(sink, JTParseError())
      }
      
    }

    return requestProducer
  }
  
  // gets all types of activities we can show in the app
  func getActivityTypes() -> SignalProducer<JTParseResponse<JTActivityType>?, JTParseError> {
    
    let requestProducer: SignalProducer<JTParseResponse<JTActivityType>?, JTParseError>
    
    let newRequest = newRequestFor(activityTypesEndpoint, withParameters: nil)
    requestProducer = makeRequest(newRequest)
    
    return requestProducer
  }
  
  // pass-through function that prepares an activity and calls addActivity
  func addActivity(ofType type: JTActivityType, fromDate start: NSDate, toDate end: NSDate) -> SignalProducer<JTParseBase?, JTParseError> {
    
    let activity = JTParseActivity()
    activity.type = JTParsePointer(type: .Pointer, className: "ActivityType", objectId: type.objectId)
    activity.startsAt = JTParseDate(type: .Date, date: start)
    activity.endsAt = JTParseDate(type: .Date, date: end)
    
    return addActivity(activity)
  }
  
  // adds new activity for currently logged in user
  func addActivity(activity: JTParseActivity) -> SignalProducer<JTParseBase?, JTParseError> {
  
    let requestProducer: SignalProducer<JTParseBase?, JTParseError>
    
    if let userId = JTUserService.sharedInstance.user.value?.objectId {
      
      activity.user = JTParsePointer(type: .Pointer, className: "_User", objectId: userId)
      if let query = Mapper().toJSONString(activity, prettyPrint: false) {
        
        let newRequest = newPOSTRequestFor(self.activitiesEndpoint, withJSONString: query)
        requestProducer = makeRequest(newRequest)
        
      } else {
        
        requestProducer = SignalProducer<JTParseBase?, JTParseError> { sink, _ in sendError(sink, JTParseError()) }
        
      }
    } else {
      
      requestProducer = SignalProducer<JTParseBase?, JTParseError> { sink, _ in sendError(sink, JTParseError()) }
      
    }
    
    return requestProducer
    
  }
  
}
