//
//  JTUserService.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa
import Alamofire
import ObjectMapper
import SwiftyUserDefaults

// MARK: Query
struct JTParseSignupQuery: Mappable {
  
  var username: String!
  var password: String!
  
  init(username: String, password: String) {
    self.username = username
    self.password = password
  }
  
  init?(_ map: Map){
  }
  
  mutating func mapping(map: Map) {
    username  <- map["username"]
    password  <- map["password"]
  }
  
}

// MARK: User service
class JTUserService: JTParseService {
  
  // Singleton
  static let sharedInstance = JTUserService()
  
  // Public properties
  var user = MutableProperty<JTUser?>(nil)
  
  // Private constants
  private let sessionEndpoint = "users/me"
  private let loginEndpoint = "login"
  private var logoutEndPoint = "logout"
  private let signupEndpoint = "users"
  private let userUpdateEndpoint = "users/"
  
  // MARK: init
  override init() {
    log.debug("")
    
    // saving credentials everytime "self.user" has changed
    user.producer
      .ignoreNil()
      .skipRepeats {(old, new) in old.sessionToken == new.sessionToken }
      .startWithNext { user in
        log.debug("user has changed")
        Defaults["username"] = user.username
        Defaults["sessionToken"] = user.sessionToken
      }
  }
  
  // MARK: Public API
  // restores session using session token stored in users default
  func restoreSesssion() -> SignalProducer<JTUser?, JTParseError>  {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    if let sessionToken = Defaults["sessionToken"].string {
      
      let newRequest = newRequestFor(sessionEndpoint, withParameters: nil)
      newRequest.setValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
      
      requestProducer = makeRequest(newRequest)
        .flatMapError {error in self.loginWithCredentials()}
        .on(next: { user in self.user.value = user })
      
    } else {
      requestProducer =  loginWithCredentials()
    }
    
    return requestProducer
  }
  
  // log in with credentials stored in user defaults
  func loginWithCredentials() -> SignalProducer<JTUser?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    if let username = Defaults["username"].string,
      let password = Defaults["password"].string {
        
        requestProducer = loginWithUsername(username, andPassword: password)
        
    } else {
      requestProducer = SignalProducer<JTUser?, JTParseError> { sink, _ in
        self.user.value = nil
        sendCompleted(sink)
      }
    }
    
    return requestProducer
  }
  
  // log out and clean settings of current user
  func logout() -> SignalProducer<JTUser?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    let sessionToken = Defaults["sessionToken"].string ?? ""
    
    let newRequest = newRequestFor(loginEndpoint, withParameters: nil)
    newRequest.setValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
    
    requestProducer = makeRequest(newRequest)
      .on(next: { user in
        self.user.value = nil
        Defaults["username"] = nil
        Defaults["sessionToken"] = nil
        Defaults["password"] = nil
        },
        error: { error in
          self.user.value = nil
          Defaults["username"] = nil
          Defaults["sessionToken"] = nil
          Defaults["password"] = nil
        }
    )
    
    return requestProducer
  }
  
  // signs up with given username and if successful gets new user's object
  func signupWithUsername(username: String, andPassword password: String) -> SignalProducer<JTUser?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    let q = JTParseSignupQuery(username: username, password: password)
    if let query = Mapper().toJSONString(q, prettyPrint: false) {
      
      let newRequest = newPOSTRequestFor(signupEndpoint, withJSONString: query)
      
      requestProducer = makeRequest(newRequest)
        .on(next: { user in
          Defaults["password"] = password
          self.user.value = user })
      
    } else {
      requestProducer = SignalProducer<JTUser?, JTParseError> { sink, _ in
        sendError(sink, JTParseError())
      }
    }
    
    return requestProducer
  }
  
  // attaches given file as user's image
  func updateUsersImage(pointer: JTParseFile?) -> SignalProducer<JTUser?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    var pointer = pointer
    pointer?.type = .File
    
    if let pointer = pointer,
      let query = Mapper().toJSONString(pointer, prettyPrint: false),
      let userId = user.value?.objectId,
      let sessionToken = Defaults["sessionToken"].string {
        
        let query = "{\"image\":\(query)}"
        let url = userUpdateEndpoint + userId
        
        let newRequest = newPUTRequestFor(url, withJSONString: query)
        newRequest.setValue(sessionToken, forHTTPHeaderField: "X-Parse-Session-Token")
        
        let updateProducer: SignalProducer<JTUser?, JTParseError> = makeRequest(newRequest)

        // after update we refresh user's object
        requestProducer = updateProducer
          .ignoreNil()
          .flatMap(.Concat) { _ in self.restoreSesssion() }
        
    } else {
      requestProducer = SignalProducer<JTUser?, JTParseError> { sink, disposable in
        sendError(sink, JTParseError())
      }
    }
    
    return requestProducer
  }
  
  // log in with username and password
  func loginWithUsername(username: String, andPassword password: String)  -> SignalProducer<JTUser?, JTParseError> {
    log.debug("")
    
    let requestProducer: SignalProducer<JTUser?, JTParseError>
    
    let parameters = [
      "username" : username,
      "password" : password
    ]
    
    let newRequest = newRequestFor(loginEndpoint, withParameters: parameters)
    
    requestProducer = makeRequest(newRequest)
      .on(next: { user in
        if let user = user {
          self.user.value = user
          Defaults["password"] = password
        }
      })
    
    return requestProducer
  }
  
}