//
//  JTreachabilityService.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 05/09/2015.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import ReactiveCocoa

// MARK: Connection Status
enum ReachabilityState: Int {
  case NotReachable = 0
  case ReachableViaWiFi = 1
  case ReachableViaWWAN = 2
}

// MARK: Reachability service
// it exposes current connection status via connectionStatus property
class JTReachabilityService {
  
  // Singleton
  static let sharedInstance = JTReachabilityService()
  
  // Public properties
  var connectionStatus = MutableProperty<ReachabilityState?>(nil)
  
  // Private property
  private let reachabilityObserver =  Reachability(hostName: "api.parse.com")
  
  // MARK: init
  init() {
    self.reachabilityObserver.startNotifier()

    self.connectionStatus <~ NSNotificationCenter.defaultCenter().rac_addObserverForName(kReachabilityChangedNotification, object: nil).toSignalProducer()
      .flatMapError { _ in SignalProducer<AnyObject?, NoError>.empty }
      .map { $0 as? NSNotification }
      .map { $0?.object as? Reachability }
      .map { $0?.currentReachabilityStatus().rawValue ?? 0 }
      .map { ReachabilityState(rawValue: $0) }

  }
  
}
