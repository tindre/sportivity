//
//  AppRouter.swift
//  Sportivity
//
//  Created by Jakub Tomanik on 04/09/15.
//  Copyright (c) 2015 Jakub Tomanik. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import XCGLogger

// This class is responsible for app-wide navidation and routing between
// different views
class AppRouter {
  
  // Singleton
  static let sharedInstance = AppRouter()
  
  //MARK: Public properties
  var error = MutableProperty<JTError?>(nil)
  
  //MARK: private properties
  private var navigationController: UINavigationController?
  
  // MARK: nit
  init(){
    
  }
  
  func createRootView() -> UIViewController {
    
    self.navigationController = UINavigationController()
    return self.navigationController!
  }
  
  // MARK: Router
  func startRouter() {
    log.debug("")
    
    // this view looks like Lunchscreen and it is a placeholder to be displayed while we ry to log in in the background
    self.showWellcomeView()
    
    // we monitor status of internet connection and show error screen when connection unavailable
    JTReachabilityService.sharedInstance.connectionStatus.producer
      .ignoreNil()
      .startWithNext { statusCode in
        if statusCode == .NotReachable {
          
          let error = JTError(type: .NoConnection)
          error.error = "Network unavailable"
          self.error.value = error
          
        } else {
          
          self.error.value = nil
        }
    }
    
    // we monitor for errors and display error screen when necessary
    self.error.producer
      .startWithNext { error in
        if let error = error {
          self.showErrorView(error)
        } else {
          self.hideErrorView()
        }
    }
    
    // we monitor network activity and show network activity indicator
    let userService = JTUserService.sharedInstance.isBusy.producer
    let backendService = JTBackendService.sharedInstance.isBusy.producer
    let fileService = JTFileService.sharedInstance.isBusy.producer
    
    // observe merged sources
    SignalProducer<SignalProducer<Bool, NoError>, NoError>(values: [userService, backendService, fileService])
      .flatten(.Merge)
      .startWithNext { visible in
        UIApplication.sharedApplication().networkActivityIndicatorVisible = visible
    }
    
    // initial flow
    // try to restore session / login with stored credentials
    // if not succesful show login view
    // if succesful show main view
    JTUserService.sharedInstance.restoreSesssion()
      //.on(error: { error in self.error.value = error })
      .flatMapError { _ in SignalProducer<JTUser?, NoError>.empty }
      .then (JTUserService.sharedInstance.user.producer)
      .startWithNext { user in
        if let user = user {
          self.showMainView(forUser: user)
        } else {
          self.showLoginView()
        }
    }
    
  }
  
  // MARK: Public methodes
  // viewmodels use them to navigate
  func showWellcomeView()
  {
    log.debug("")
    
    let wellcomeViewController = JTReactiveViewController(nibName: "WellcomeView", bundle: nil)
    self.navigationController?.setViewControllers([wellcomeViewController], animated: false)
  }
  
  func showErrorView(error: JTError?) {
    log.debug("")
    
    if let error = error {
      let errorViewModel = JTErrorViewModel(message: error)
      let errorViewController = JTErrorViewController(viewmodel: errorViewModel)
      
      if error.type == .NoConnection {
        
        self.navigationController?.presentViewController(errorViewController, animated: true) {}
      } else {
        
        self.navigationController?.pushViewController(errorViewController, animated: true)
      }
    }
    
  }
  
  func hideErrorView() {
    if self.navigationController?.visibleViewController is JTErrorViewController {
      self.navigationController?.dismissViewControllerAnimated(true, completion: {} )
    }
  }
  
  func showLoginView() {
    log.debug("")
    
    let loginViewController = JTLoginViewController(viewmodel: JTLoginViewModel())
    self.navigationController?.setViewControllers([loginViewController], animated: false)
  }
  
  func showSignUpView() {
    log.debug("")
    
    let signupViewController = JTSignUpViewController(viewmodel: JTSignupViewModel())
    self.navigationController?.pushViewController(signupViewController, animated: true)
  }
  
  func showMainView(forUser user: JTUser) {
    log.debug("")
    
    let mainViewModel = JTMainViewModel(user: user)
    let mainViewController = JTMainViewController(viewmodel: mainViewModel)
    self.navigationController?.setViewControllers([mainViewController], animated: false)
  }
  
  func showAddActivity(date: NSDate) {
    log.debug("")
    
    let addActivityViewController = JTAddActivityViewController(viewmodel: JTAddActivityViewModel(date: date))
    self.navigationController?.pushViewController(addActivityViewController, animated: true)
  }
  
  func hideAddActivity() {
    log.debug("")
    self.navigateBack()
  }
  
  func navigateBack() {
    log.debug("")
    self.navigationController?.popViewControllerAnimated(true)
  }
  
}